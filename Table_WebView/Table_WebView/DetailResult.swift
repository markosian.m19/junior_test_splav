//
//  DetailResult.swift
//  Table_WebView
//
//  Created by Марат Маркосян on 09.02.2022.
//

import UIKit

class DetailResult: UIViewController {
    
    private lazy var leafImage = UIImageView(image: UIImage(systemName: Constants.leaf))
    private lazy var nameLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSubviews()
        setUpAutoLayout()
    }
    
    private func setUpSubviews() {
        view.backgroundColor = .systemBackground
        
        view.addSubview(leafImage)
        view.addSubview(nameLabel)
        
        leafImage.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        nameLabel.text = Constants.leaf.capitalized
        nameLabel.font = UIFont.boldSystemFont(ofSize: Constants.fortyCG)
    }
    
    private func setUpAutoLayout() {
        NSLayoutConstraint.activate([
            leafImage.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            leafImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            leafImage.widthAnchor.constraint(equalToConstant: Constants.hundredCG),
            leafImage.heightAnchor.constraint(equalToConstant: Constants.hundredCG),
            
            nameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            nameLabel.topAnchor.constraint(equalTo: leafImage.bottomAnchor)
        ])
    }

}
