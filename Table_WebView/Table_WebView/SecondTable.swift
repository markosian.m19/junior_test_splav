//
//  SecondTable.swift
//  Table_WebView
//
//  Created by Марат Маркосян on 10.02.2022.
//

import UIKit

class SecondTable: UIViewController {

    private lazy var customTable = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSubviews()
        setUpAutoLayout()
    }
    
    private func setUpSubviews() {
        view.backgroundColor = .systemBackground
                
        customTable.dataSource = self
        customTable.delegate = self
        
        view.addSubview(customTable)
        customTable.translatesAutoresizingMaskIntoConstraints = false
        
        customTable.register(CustomCell.self, forCellReuseIdentifier: Constants.reusableCellIdnt)
    }
    
    private func setUpAutoLayout() {
        NSLayoutConstraint.activate([
            customTable.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            customTable.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            customTable.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            customTable.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

}

extension SecondTable: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.five
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = customTable.dequeueReusableCell(withIdentifier: Constants.reusableCellIdnt) as? CustomCell {
            cell.labelTxt.isHidden = true
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension SecondTable: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let presentedVC = DetailResult()
        present(presentedVC, animated: true, completion: nil)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
