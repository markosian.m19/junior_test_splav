//
//  WebViewResult.swift
//  Table_WebView
//
//  Created by Марат Маркосян on 10.02.2022.
//

import UIKit
import WebKit

class WebViewResult: UIViewController {
    
    private lazy var web = WKWebView()
    
    override func loadView() {
        super.loadView()
        
        web.navigationDelegate = self
        view = web
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        
        if let url = URL(string: Constants.urlForWeb) {
            web.load(URLRequest(url: url))
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        do {
            let contents = try String(contentsOf: web.url!)
            if contents == Constants.mousecheese || contents == Constants.emptyString {
                let secondVC = SecondTable()
                secondVC.modalPresentationStyle = .fullScreen
                present(secondVC, animated: true, completion: nil)
            }
        } catch {
            print(error)
        }
    }
    
    
    
}

extension WebViewResult: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = web.title
    }
    
}
