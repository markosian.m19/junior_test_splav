//
//  Constants.swift
//  Table_WebView
//
//  Created by Марат Маркосян on 10.02.2022.
//

import UIKit

struct Constants {
    
    //MARK: - Strings
    static let fortuneTitle = "Fortune"
    static let reusableCellIdnt = "ReusableCell"
    static let urlForWeb = "https://melioriuqn.ru/HpVz1XHM"
    static let mousecheese = "mousecheese"
    static let leaf = "leaf"
    static let question = "?"
    
    //MARK: - Numbers
    static let five = 5
    static let sixtyCG: CGFloat = 60
    static let fortyCG: CGFloat = 40
    static let twentyFont: CGFloat = 20
    static let fifteenCG: CGFloat = 15
    static let tenCG: CGFloat = 10
    static let hundredCG: CGFloat = 100
    static let emptyString = ""
    
}
