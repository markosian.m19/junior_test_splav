//
//  CustomCell.swift
//  Table_WebView
//
//  Created by Марат Маркосян on 09.02.2022.
//

import UIKit

class CustomCell: UITableViewCell {
    
    lazy var labelTxt = UILabel()
    lazy var leafLabel = UILabel()
    lazy var leafImage = UIImageView(image: UIImage(systemName: Constants.leaf))
    
    override func awakeFromNib() {
        super.awakeFromNib()


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        labelTxt.translatesAutoresizingMaskIntoConstraints = false
        leafImage.translatesAutoresizingMaskIntoConstraints = false
        leafLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(labelTxt)
        addSubview(leafLabel)
        addSubview(leafImage)
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Constants.sixtyCG),
            
            labelTxt.centerXAnchor.constraint(equalTo: centerXAnchor),
            labelTxt.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            leafImage.widthAnchor.constraint(equalToConstant: Constants.fortyCG),
            leafImage.heightAnchor.constraint(equalToConstant: Constants.fortyCG),
            leafImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            leafImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.tenCG),
            
            leafLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.fifteenCG),
            leafLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        labelTxt.text = Constants.question
        leafLabel.text = Constants.leaf.capitalized
        leafLabel.font = UIFont.boldSystemFont(ofSize: Constants.twentyFont)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    
        
    }
    
}
